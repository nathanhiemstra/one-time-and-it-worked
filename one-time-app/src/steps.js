import React from 'react';
import ReactDOM from 'react-dom';
import '../node_modules/bootstrap/dist/css/bootstrap.min.css';
import './scss/app.scss';
// import App from './App';
import reportWebVitals from './reportWebVitals';



class StepButton extends React.Component {
  
  constructor(props) {
    super(props);
    this.handleClick = this.handleClick.bind(this);
  }

  handleClick() {
    this.props.onStepChange();
  }

  render() {    
    let disabled = false;
    let label;
    if (this.props.stepDirection === 'next') {
      label = 'Next';
      disabled = this.props.step > 5;
    } else if (this.props.stepDirection === 'back') {
      label = 'Back';
      disabled = this.props.step < 2;
    }
    
    return (
      <button onClick={this.handleClick} disabled={disabled} className="btn btn-primary me-3">
        {label}
      </button>
    );
  }
}


class App extends React.Component {
  constructor(props) {
    super(props);
    this.handleStepNext = this.handleStepNext.bind(this);
    this.handleStepBack = this.handleStepBack.bind(this);
    this.state = {step: 1};
  }

  handleStepNext() {
    this.setState({step: this.state.step + 1});
  }

  handleStepBack() {
    if (this.state.step < 2) {
      return null;
    }
    this.setState({step: this.state.step - 1});
  }

  render() {
    return(
      <main className="container app" step={this.state.step}>

        <ol class="step-list mb-6">
          <li class="step-1-vh">Name two objects</li>
          <li class="step-2-vh">
            What’s the <strong class="normal">most normal</strong> thing done to “<span class="output-object-1">object 1</span>” 
            &nbsp;...but <strong class="never">never</strong> done to “<span class="output-object-2">object 2</span>”?
          </li>
          <li class="step-3-vh">What’s the <strong class="normal">most normal</strong> thing done to “<span class="output-object-2">object 2</span>” ...but <strong class="never">never</strong> done to “<span class="output-object-1">object 1</span>”?</li>
          <li class="step-4-vh">Make it past tense. 
            <span class="suggestion d-none">
              Prepesitions or adjectives? 
              <span class="small">
                i.e. “<em>My yummy</em> <span class="output-object-1">object 1</span>”
              </span>
            </span>

          </li>
          <li class="step-5-vh">
            Review and cleanup.  
          </li>
          <li class="step-6-vh">Customize and share</li>
        </ol>

        <section class="overflow-hidden">
          <div class="sentence step-6-hidden">
            <div class="item1 sentence__i        sentence__prefix">I</div>
            <textarea class="item2 sentence__verb-1   sentence__input"  placeholder="Verb 1" role="textbox" contenteditable>Verb 1 property to set a defauproperty to set a defauproperty to set a defauproperty to set a defauproperty to set a defauproperty to set a defauproperty to set a defaut</textarea>
            <div class="item3 sentence__arrow  | step-2-flex step-3-flex step-2-opacity step-3-opacity"><img src="images/arrows-top-sm.svg" class="sentence__arrow-item" /></div>  
            <textarea class="item4 sentence__object-1 sentence__input  | sentence__input-object" placeholder="Object 1" role="textbox" contenteditable></textarea>

            <div class="item5 sentence__and      sentence__prefix | step-3-up-opacity">and</div>
            <textarea class="item6 sentence__verb-2   sentence__input  | step-3-up-opacity" placeholder="Verb 2" role="textbox" contenteditable></textarea>
            <textarea class="item8 sentence__object-2 sentence__input  | sentence__input-object" placeholder="Object 2" role="textbox" contenteditable></textarea>
          </div>
        </section>



        {/* <section class="overflow-hidden">
          <div class="sentence step-6-hidden">
            <div class="item1 sentence__i        sentence__prefix">I</div>
            <div class="item2 sentence__verb-1   sentence__input"  placeholder="Verb 1" role="textbox" contenteditable>Verb 1 property to set a defauproperty to set a defauproperty to set a defauproperty to set a defauproperty to set a defauproperty to set a defauproperty to set a defaut</div>
            <div class="item3 sentence__arrow  | step-2-flex step-3-flex step-2-opacity step-3-opacity"><img src="images/arrows-top-sm.svg" class="sentence__arrow-item" /></div>  
            <div class="item4 sentence__object-1 sentence__input  | sentence__input-object" placeholder="Object 1" role="textbox" contenteditable></div>

            <div class="item5 sentence__and      sentence__prefix | step-3-up-opacity">and</div>
            <div class="item6 sentence__verb-2   sentence__input  | step-3-up-opacity" placeholder="Verb 2" role="textbox" contenteditable></div>
            <div class="item8 sentence__object-2 sentence__input  | sentence__input-object" placeholder="Object 2" role="textbox" contenteditable></div>
          </div>
        </section>
        */}

        <section class="mt-3">
          <StepButton
            stepDirection={'back'}
            onStepChange={this.handleStepBack}
            step={this.state.step} />
          <StepButton
            stepDirection={'next'}
            onStepChange={this.handleStepNext}
            step={this.state.step} />
{/* 

          <button type="submit" class="btn btn-primary" id="btn-back" disabled>Back</button>
          <button type="submit" class="btn btn-primary step-6-hidden" id="btn-next">Next</button> */}
          <button type="submit" class="btn btn-primary step-6" id="btn-reset">Start Over</button>
          <button type="submit" class="btn btn-dark step-6" id="btn-share" data-toggle="modal" data-target="#shareModal">Share</button>
        </section>


      </main>



    );
  }
}


  // =================

  ReactDOM.render(
    <App  />,
    document.getElementById('root')
  )

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();

