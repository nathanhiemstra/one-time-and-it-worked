import React from 'react';
import ReactDOM from 'react-dom';
import '../node_modules/bootstrap/dist/css/bootstrap.min.css';
import './scss/app.scss';
// import App from './App';
import reportWebVitals from './reportWebVitals';




class Clock extends React.Component {
  
  constructor(props) {
    super(props);
    // Define the state with current time.
    this.state = {date: new Date()};
  }

  componentDidMount() {
    // Make an ID that calls a function every second 
    this.timerID = setInterval(
      () => this.tick(),
      1000
    );
  }

  componentWillUnmount() {
    clearInterval(this.timerID);
  }

  tick() {
    // THis is the fucntion. It makes a new "now" time every time called.
    this.setState({
      date: new Date()
    });
  }

  render() {
    return (
      <div>
        <h1>Hello, world!</h1>
        // The state gets pushed here.
        <h2>It is {this.state.date.toLocaleTimeString()}.</h2>
      </div>
    );
  }
}


  // =================

  ReactDOM.render(
    <Clock  />,
    document.getElementById('root')
  )

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();


